# Running for Developement
For running the API in development you first need to spin-up the database.
```bash
docker-compose up -d
```

The `python-mysqlclient` package is required for the application to run.
This package can be installed in Manjaro from `Add/Remove Software`
or with
```bash
sudo pacman -S python-mysqlclient
```

Create the database structure
```bash
alembic -c migrations/alembic.ini upgrade head
```

And then run the fastapi within the poetry enviroment
```bash
poetry shell
fastapi dev main.py
```

# Running for Production
1. Set the environment vars correctly with the .env file
2. Login to the terminal of the server to install all packages
```bash
pip install -r requirements.txt
```
3. Upgrade/Create the database
```bash
alembic -c migrations/alembic.ini upgrade head
```
4. Start the server, see documentation https://fastapi.tiangolo.com/deployment/
