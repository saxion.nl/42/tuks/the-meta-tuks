from fastapi import Depends, HTTPException
from fastapi.routing import APIRouter
from fastapi.security import OAuth2PasswordBearer, OAuth2PasswordRequestForm
from models.database import get_session
from sqlalchemy.orm import Session
from models.user import authenticate_user, get_user, set_token_by_email, get_user_by_token

from utils import getenv, random_hash
import jwt
from typing import Annotated
from pydantic import BaseModel, EmailStr
from datetime import datetime, timedelta, UTC

SECRET_KEY = getenv("SECRET_KEY", "")
SECRET_ALGORITHM = getenv("SECRET_ALGORITHM", "")
ACCESS_TOKEN_EXPIRE_MINUTES = int(getenv("SECRET_TOKEN_EXPIRES")) # type: ignore
TOKEN_LENGTH = int(getenv("TOKEN_LENGTH", 32))

URL = "https://s.tuks.dev/reset-password/"

auth_router = APIRouter(prefix="/token", tags=['Authentication'])

oauth2_scheme = OAuth2PasswordBearer(tokenUrl="token")

# Mail
from fastapi_mail import FastMail, MessageSchema, MessageType, ConnectionConfig

MAIL_CONFIG = ConnectionConfig(
    MAIL_FROM=getenv("MAIL_FROM"),
    MAIL_FROM_NAME=getenv("MAIL_FROM_NAME"),
    MAIL_USERNAME=getenv("MAIL_USERNAME"), # type: ignore
    MAIL_PASSWORD=getenv("MAIL_PASSWORD"), # type: ignore
    MAIL_PORT=int(getenv("MAIL_PORT", 25)),
    MAIL_SERVER=getenv("MAIL_SERVER", 'localhost'),
    MAIL_SSL_TLS=False,
    MAIL_STARTTLS=False
)


def generate_mail(username, link):
    return f"""
Dear {username},

We received a request to reset the password associated with your Tuks Study Association account. If you initiated this request, please click the link below to reset your password:

{link}

If you did not request a password reset, please ignore this email. Your account security is important to us, so if you have any concerns, feel free to contact our support team.

Best regards,
Study Association S.V. Tuks
"""

class Token(BaseModel):
    access_token: str
    token_type: str


class TokenPayload(BaseModel):
    id: int
    name: str
    email: str
    roles: dict | None
    discount: float
    allowed_prohibited: bool
    exp: int = 0


def generate_token(data: dict):
    expire = datetime.now(UTC) + timedelta(minutes=ACCESS_TOKEN_EXPIRE_MINUTES)
    data['exp'] = expire
    return jwt.encode(data, SECRET_KEY, SECRET_ALGORITHM)


def verify_token(token: str):
    try:
        jwt.decode(token, SECRET_KEY, algorithms=[SECRET_ALGORITHM])
    except Exception as e:   # noqa: BLE001
        print(e)
        return False
    return True


def get_current_user(
    token: str = Depends(oauth2_scheme), 
    session: Session = Depends(get_session)
) -> dict:
    try:
        payload: TokenPayload = jwt.decode(token, SECRET_KEY, algorithms=[SECRET_ALGORITHM])
        user_id = payload["id"]
        
        user = get_user(session, user_id)
        if not user:
            return payload
        
        payload["allowed_prohibited"] = user.allowed_prohibited
        payload["roles"] = user.roles

        return payload
    except jwt.PyJWTError:
        raise HTTPException(status_code=401, detail="Invalid JWT Token")



def is_board(payload: dict = Depends(get_current_user)):
    if payload['roles'] and 'board' in payload['roles']:
        return True
    raise HTTPException(status_code=403, detail="STUKS: Invalid role for using this endpoint")


def is_fridge(payload: dict = Depends(get_current_user)):
    if payload['roles'] and 'fridge' in payload['roles']:
        return True
    raise HTTPException(status_code=403, detail="STUKS: Invalid role for using this endpoint")

def is_board_or_fridge(payload: dict = Depends(get_current_user)):
    if payload['roles'] and ('board' in payload['roles'] or 'fridge' in payload['roles']):
        return True
    raise HTTPException(status_code=403, detail="STUKS: Invalid role for using this endpoint")

def is_sysco(payload: dict = Depends(get_current_user)):
    if payload['roles'] and 'sysco' in payload['roles']:
        return True
    raise HTTPException(status_code=403, detail="STUKS: Invalid role for using this endpoint")


def is_dnd(payload: dict = Depends(get_current_user)):
    if payload['roles'] and 'dnd' in payload['roles']:
        return True
    raise HTTPException(status_code=403, detail="STUKS: Invalid role for using this endpoint")


def is_treasure(payload: dict = Depends(get_current_user)):
    if payload['roles'] and 'treasure' in payload['roles']:
        return True
    raise HTTPException(status_code=403, detail="STUKS: Invalid role for using this endpoint")


def is_activity(payload: dict = Depends(get_current_user)):
    if payload['roles'] and 'activity' in payload['roles']:
        return True
    raise HTTPException(status_code=403, detail="STUKS: Invalid role for using this endpoint")


@auth_router.post("")
async def login_for_access(form_data: Annotated[OAuth2PasswordRequestForm, Depends()], session: Session = Depends(get_session)) -> Token | None:
    user = authenticate_user(session, form_data)
    if user is None:
        raise HTTPException(status_code=401, detail="STUKS: email and/or password are not correct.")
    payload = TokenPayload(
        id=user.id, # type: ignore
        email=user.email, # type: ignore
        roles=user.roles, # type: ignore
        discount=user.discount(),
        allowed_prohibited=bool(user.allowed_prohibited),
        name=user.name # type: ignore
    )
    if user:
        return Token(access_token=generate_token(payload.model_dump()), token_type='Bearer')
    return None


class PasswordResetModel(BaseModel):
    email: EmailStr


@auth_router.post("reset-password", description="Change your password by known email", status_code=200, response_description="Email send with reset token")
async def reset_password(request: PasswordResetModel, session: Session = Depends(get_session)):
    # Clear password, reset token
    token = random_hash(TOKEN_LENGTH)

    user = set_token_by_email(session, request.email, token)
    if not user:
        raise HTTPException(404, detail="User not found by email")
    # Mail user with reset-token
    message = MessageSchema(
        subject="Password reset",
        recipients=[request.email],
        subtype=MessageType.plain,
        body=generate_mail(user.name, f"{URL}{token} \r\n\r\nToken: {token}")
    )
    await FastMail(MAIL_CONFIG).send_message(message)


class PasswordModel(BaseModel):
    password: str


@auth_router.post("reset-password/{token}", description="Change password with retrieved token by email", status_code=200, response_description="Password updated, ready for login.")
def apply_password(request: PasswordModel, token: str, session: Session = Depends(get_session)):
    if not get_user_by_token(session, token, request.password):
        raise HTTPException(406, detail="Email or token are not correct")
