from utils import getenv
from fastapi import Depends, Header, HTTPException
from fastapi.routing import APIRouter
from typing import Annotated
from models.database import Session, get_session
from models.purchase import PurchasePublic, purchase_all
from models.user import UserPublic
from models.product import ProductFull

ANOM_SECRET_KEY = getenv("ANOM_SECRET_KEY")

purchase_router = APIRouter(prefix='/purchases', tags=['Purchases'])


async def verify_anom_secret(anom_secret: Annotated[str, Header()]):
    if anom_secret != ANOM_SECRET_KEY:
        raise HTTPException(status_code=401, detail="Not allowed to hit the endpoint")


@purchase_router.get("", description="All purchases (sorted default by date)", dependencies=[Depends(verify_anom_secret)])
def get_all_purchases(limit: int = 20, session: Session = Depends(get_session)):
    purchases = purchase_all(session, limit)
    if not purchases:
        raise HTTPException(status_code=204, detail="Not allowed to hit the endpoint")
    
    return [PurchasePublic(**purchase.__dict__, product=ProductFull(**purchase.product.__dict__), user=UserPublic(**purchase.user.__dict__)) for purchase in purchases]
