from models.database import Session, get_session, Base, engine
from fastapi.routing import APIRouter
from fastapi import Depends

from models.user import User
from models.product import Product
from models.purchase import Purchase

import random
import uuid

dev_router = APIRouter(prefix="/dev", tags=['Development'])


@dev_router.get('/reset-database', description="Endpoint for development-purpose")
def reset_database(session: Session = Depends(get_session)):
    Base.metadata.drop_all(engine)
    Base.metadata.create_all(engine)

    # Populate special users
    special_users = {
        'Board_user':
        {'email': 'board_user@tuks.dev', 'password': 'P@ssw0rd', 'roles': {'board': 0.10}, 'allowed_prohibited': True},
        'Sysco_user':
        {'email': 'sysco_user@tuks.dev', 'password': 'P@ssw0rd', 'roles': {'sysco': 0.10}, 'allowed_prohibited': True},
        'Fridge_user':
        {'email': 'Fridge_user@tuks.dev', 'password': 'P@ssw0rd', 'roles': {'fridge': 0.10}, 'allowed_prohibited': True},
        'Treasurer_user':
        {'email': 'treasurer_user@tuks.dev', 'password': 'P@ssw0rd', 'roles': {'treasure': 0.10, 'board': 0.10}, 'allowed_prohibited': True}
    }

    for name, attributes in special_users.items():
        add_user = User()
        add_user.name = name
        add_user.email = attributes['email']
        add_user.set_password(attributes['password'])
        add_user.roles = attributes['roles']
        add_user.avatar = "/static/DFD17D37Ba6fbb0C.jpg"
        add_user.allowed_prohibited = attributes.get('prohibited', False)
        session.add(add_user)
        session.commit()
        session.refresh(add_user)
        special_users[name] = add_user


    # Populate members
    for member in range(1, 11):
        user = User()
        user.name = f"Member_{member}"
        user.email = f"{user.name.lower()}@tuks.dev"
        if member % 2:
            user.avatar = "/static/DFD17D37Ba6fbb0C.jpg"
        user.allowed_prohibited = bool(member % 3 == 0)
        user.set_password("P@ssw0rd")
        session.add(user)
        session.commit()

    # Populate products
    for product in range(1, 11):
        prod = Product()
        prod.name = f"{random.choice(['Drink', 'Snack'])}_{product}"
        prod.price = round(random.uniform(0.15, 1.20), 2)
        prod.stock = random.randint(10, 100)
        prod.ean_code = str(uuid.uuid4().int)[:13]
        prod.prohibited = bool(product % 2 == 0)
        prod.type = prod.name.lower().split('_')[0]
        if product % 2:
            prod.picture = "/static/C0bbf6aB73D71DFD.jpg"
        session.add(prod)
        session.commit()

    # Populate random purchases
    for _ in range(80):
        user = random.choice(session.query(User).all())
        product = random.choice(session.query(Product).all())
        purchase = Purchase()
        purchase.user_id = user.id
        purchase.product_id = product.id
        purchase.price = round(product.price * (1 - user.discount()), 2)
        session.add(purchase)
        session.commit()

    return {'status': 200, "message": "Database is populated"}
