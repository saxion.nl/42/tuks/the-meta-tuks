from fastapi.routing import APIRouter
from fastapi import Depends
from models.purchase import mark_unpaid_by_user_as_paid, PurchaseInvoice, purchase_all, PurchaseFull
from models.database import Session, get_session
from routes.auth import is_treasure

administration_router = APIRouter(prefix='/administration', tags=['Administration'])


@administration_router.put('/{user_id}/paid', dependencies=[Depends(is_treasure)], response_model=PurchaseInvoice)
def mark_user_purchases_paid(user_id: int, invoice: int, session: Session = Depends(get_session)):
    return mark_unpaid_by_user_as_paid(session, user_id, invoice)
