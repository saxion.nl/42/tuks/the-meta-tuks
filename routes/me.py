from models.database import Session, get_session
from routes.auth import get_current_user
from fastapi.routing import APIRouter
from fastapi import Depends, UploadFile, HTTPException

from models.user import UserPrivate, get_user, user_avatar, change_password, ChangePassword
from models.purchase import PurchaseFull, purchase_product, purchase_user

from utils import random_filename

me_router = APIRouter(prefix='/me', tags=['Me'])


@me_router.get("", response_model=UserPrivate, description="Show my own profile")
def me_profile(session: Session = Depends(get_session), current_user = Depends(get_current_user)):
    return get_user(session, current_user['id'])


@me_router.patch("", description="upload a avatar for your own profile")
async def update_avatar(file: UploadFile, current_user=Depends(get_current_user), session: Session = Depends(get_session)):
    if not file or not file.filename:
        return {'status': 'Not given correct file or filename'}
    new_filename = random_filename(file.filename)
    with open(f"static/{new_filename}", 'wb+') as writer:
        writer.write(file.file.read())
    user_avatar(session, current_user['id'], new_filename)
    return {'status': 'File uploaded correctly', 'file': f"/static/{new_filename}"}

@me_router.get("/purchase", description="Show all purchases, default the last 100, ordered by purchase date desc.")
def me_purchased(limit: int = 100, session: Session = Depends(get_session), current_user = Depends(get_current_user)):
    purchases = purchase_user(session, current_user['id'], limit)
    if not purchases:
        return {'status': 'No purchases found'}
    return [PurchaseFull(**purchase.__dict__) for purchase in purchases]


@me_router.post("/purchase", description="Purchase a product")
def me_buy_product(product_id: int, session: Session = Depends(get_session), current_user = Depends(get_current_user)):
    result = purchase_product(session, current_user, product_id)
    
    return PurchaseFull(**result.__dict__)


@me_router.patch("/change-password", description="Change your password, only when loggedin", status_code=202)
def me_change_password(password_change: ChangePassword, session: Session = Depends(get_session), current_user=Depends(get_current_user)):
    if password_change.password != password_change.repeat:
        raise HTTPException(status_code=400, detail="Passwords don't match")
    change_password(password_change.password, session, current_user['id'])
