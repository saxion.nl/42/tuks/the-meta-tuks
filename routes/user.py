from fastapi import Depends, HTTPException

from fastapi.routing import APIRouter
import models.user as User
from models.database import get_session
from routes.auth import is_board
from sqlalchemy.orm import Session

user_router = APIRouter(prefix='/users', tags=['Users'])


@user_router.get("", description="Show only active users", response_model=list[User.UserPublic])
def retrieve_active_users(session: Session = Depends(get_session)) -> list[User.UserPublic]:
    return User.get_active_users(session)


@user_router.post("", dependencies=[Depends(is_board)], status_code=201)
def create_new_user(user: User.UserNew, session: Session = Depends(get_session)) -> User.UserFull:
    return User.create_user(session, user)


@user_router.get("/all", dependencies=[Depends(is_board)])
def retrieve_all_users(session: Session = Depends(get_session)) -> list[User.UserPrivate]:
    return User.get_all_users_with_purchases(session)


@user_router.get('/roles', description="Retrieve possible roles with the corresponding discount")
def roles():
    return User.ROLES


@user_router.get("/{user_id}", dependencies=[Depends(is_board)], response_model=User.UserFull)
def retrieve_certain_user(user_id: int, session: Session = Depends(get_session)) -> User.UserFull:
    result = User.get_user(session, user_id)
    
    if not result:
        raise HTTPException(404, detail="User not found")
    return result


@user_router.put("/{user_id}", dependencies=[Depends(is_board)], response_model=User.UserFull)
def update_profile(user_id: int, profile: User.UserProfile, session: Session = Depends(get_session)) -> User.UserFull:
    result = User.update_profile(session, user_id, profile)
    if not result:
        raise HTTPException(404, detail="User not found")
    return result


@user_router.patch("/{user_id}/moneybird", dependencies=[Depends(is_board)])
def connect_to_moneybird(user_id: int, moneybird_id: str, session: Session = Depends(get_session)):
    return User.set_user_moneybird(session, user_id, moneybird_id)


@user_router.patch("/{user_id}/inactive", dependencies=[Depends(is_board)])
def disable_user(user_id: int, session: Session = Depends(get_session)):
    return User.set_user_inactive(session, user_id)


@user_router.patch("/{user_id}/active", dependencies=[Depends(is_board)])
def enable_user(user_id: int, session: Session = Depends(get_session)):
    return User.set_user_active(session, user_id)


@user_router.patch('/{user_id}/role', description="Adds a role to the given user.", dependencies=[Depends(is_board)])
def add_role(user_id: int, role: str, session: Session = Depends(get_session)):
    return User.set_user_role(session, user_id, role)

@user_router.delete('/{user_id}/role', description="Removes the given role from the given user.", dependencies=[Depends(is_board)])
def remove_role(user_id: int, role: str, session: Session = Depends(get_session)):
    return User.remove_user_role(session, user_id, role)

@user_router.patch("/{user_id}/allowed_prohibited", dependencies=[Depends(is_board)])
def set_allowed_prohibited(user_id: int, allowed: bool, session: Session = Depends(get_session)):

    return User.set_allowed_prohibited(session, user_id, allowed)
