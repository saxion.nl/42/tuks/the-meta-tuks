from fastapi import Depends, HTTPException, UploadFile
from fastapi.routing import APIRouter
from models.database import get_session
from models.product import product_create, products_active, product_get_by_id, product_update, product_enable, product_disable, all_products, ProductCreate, ProductFull, product_update_picture, ProductStockUpdate, product_stock_update
from sqlalchemy.orm import Session
from routes.auth import TokenPayload, is_board, get_current_user, is_board_or_fridge
from utils import random_filename

product_router = APIRouter(prefix='/products', tags=['Products'])


@product_router.get("", description="All products (filtered by user permissions)")
def get_active_products(
    session: Session = Depends(get_session),
    user_payload = Depends(get_current_user)
) -> list[ProductFull]:
    allowed_prohibited = user_payload["allowed_prohibited"]
    products = products_active(session, allowed_prohibited)

    return [ProductFull(**product.__dict__) for product in products]

@product_router.patch("/update-stock", description="Update the stock with the given amount", dependencies=[Depends(is_board_or_fridge)])
def update_stock(product_stock: ProductStockUpdate, current_user=Depends(get_current_user), session: Session = Depends(get_session)):
    result = product_stock_update(session, product_stock, current_user)
    if not result:
        raise HTTPException(status_code=304, detail="Product not found")
    return HTTPException(202, detail=f"Product updated to {result}.")

@product_router.post("", dependencies=[Depends(is_board)])
def new_product(product: ProductCreate, session: Session = Depends(get_session)) -> ProductFull:
    prod = product_create(session, product)
    return ProductFull(**prod.__dict__)

@product_router.get("/all", dependencies=[Depends(is_board)])
def get_all_products(session: Session = Depends(get_session)) -> list[ProductFull]:
    return [ProductFull(**product.__dict__) for product in all_products(session)]


@product_router.get("/{product_id}")
def get_product(product_id: int, session: Session = Depends(get_session)) -> ProductFull:
    product = product_get_by_id(session, product_id)
    if product is None:
        raise HTTPException(status_code=404, detail=f"Product requested by id {product_id} is not found")
    return ProductFull(**product.__dict__)


@product_router.post("/{product_id}/picture", description="upload a picture to corresponding product", dependencies=[Depends(is_board)])
def product_picture(product_id: int, file: UploadFile, session=Depends(get_session)):
    if not file or not file.filename:
        raise HTTPException(status_code=400, detail="File not uploaded or recognized")
    hashed_filename = random_filename(file.filename)
    with open(f"static/{hashed_filename}", 'wb+') as writer:
        writer.write(file.file.read())
    product_update_picture(session, product_id, f"/static/{hashed_filename}")
    return {'status': 'File uploaded correctly', 'file': f"/static/{hashed_filename}"}


@product_router.put("/{product_id}", dependencies=[Depends(is_board)])
def update_product(product_id: int, product: ProductFull, session: Session = Depends(get_session)):
    prod = product_update(session, product_id, product)
    if prod is None:
        raise HTTPException(status_code=404, detail=f"Product update by id {product_id} is not found")
    return ProductFull(**prod.__dict__)


@product_router.patch("/{product_id}/disable", dependencies=[Depends(is_board)])
def disable_product(product_id: int, session: Session = Depends(get_session)):
    return product_disable(session, product_id)


@product_router.patch("/{product_id}/enable", dependencies=[Depends(is_board)])
def enable_product(product_id: int, session: Session = Depends(get_session)):
    return product_enable(session, product_id)
