from dotenv import load_dotenv
from os import getenv
from random import choice
import string
import logging

FILENAME_LENGTH = 16
if not load_dotenv():
    raise FileNotFoundError("The .env file is not found")


def random_hash(length: int) -> str:
    return ''.join(choice(string.hexdigits) for _ in range(length))


def random_filename(original_filename: str) -> str:
    extension = original_filename.split('.')[-1]
    return random_hash(FILENAME_LENGTH) + "." + extension


def get_logger():
    logging.basicConfig(
        filename='audit.log',
        filemode='a',
        encoding='utf-8',
        format="{asctime} - {levelname} - {message}",
        style='{',
        datefmt="%Y-%m-%d %H:%M",
        level=logging.INFO
    )
    return logging
