from sqlalchemy import Column, DateTime, String, ForeignKey, Integer, Text
from .product import Product


class Event(Product):
    __tablename__ = "events"
    
    id = Column(Integer, ForeignKey("product.id"), primary_key=True, autoincrement="ignore_fk")
    start_time = Column(DateTime, nullable=False)
    description = Column(Text, default="")
    sign_up_until = Column(DateTime, nullable=False)
