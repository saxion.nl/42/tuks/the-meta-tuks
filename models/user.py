from .database import Base
from sqlalchemy import Column, Integer, Boolean, Text, JSON
from sqlalchemy.orm import Session, Relationship
from pydantic import BaseModel, ConfigDict, Field
from passlib.context import CryptContext
from fastapi.logger import logger
from fastapi.security import OAuth2PasswordRequestForm

password_context = CryptContext(schemes=["bcrypt"])

# Key = Name, Value = discount (10% == 0.10)
ROLES = {
    'board': 0.10,
    'sysco': 0.10,
    'fridge': 0.10,
    'treasure': 0.10,
    'dnd': 0.10,
    'activity': 0.10
}


class User(Base):
    __tablename__ = 'users'

    id = Column(Integer, primary_key=True)
    name = Column(Text, nullable=False)
    email = Column(Text, unique=True, index=True)
    __password = Column("password", Text, nullable=False, ) # Saved with encryption, see routes/auth
    is_active = Column(Boolean, default=True)
    allowed_prohibited = Column(Boolean, default=False)
    moneybird_id = Column(Text, nullable=True)
    avatar = Column(Text, nullable=True)
    roles = Column(JSON)
    reset_token = Column(Text, nullable=True)

    purchases = Relationship('Purchase')

    def verify_password(self, password: str) -> bool:
        return password_context.verify(password, str(self.__password))

    def set_password(self, password: str):
        self.__password = password_context.hash(password)

    def verify_token(self, token: str) -> bool:
        """Check if the given token is correct by this user

        Args:
            token (str): token given by the user

        Returns:
            bool: True if the token is correct, False if token is incorrect. In both scenarios the token is cleared 
        """
        saved_token = str(self.reset_token)
        self.reset_token = ""
        return saved_token == token
    
    def set_token(self, token: str):
        """Clean password and insert reset_token

        Args:
            token (str): token that's used in email for verification
        """
        self.__password = ""
        self.reset_token = token

    def discount(self) -> float:
        """Maximum amount of discount

        Returns:
            float: 10% is 0.10
        """
        discount = 0.00

        if self.roles: # type: ignore
            for role_discount in self.roles.values():
                discount = max(role_discount, discount)

        return 1 - discount

    def spend(self):
        total_amount = 0.0
        for purchase in self.purchases:
            total_amount += purchase.price
        return total_amount

    def debt(self):
        total_amount = 0.0
        for purchase in self.purchases:
            if purchase.invoice is None:
                total_amount += purchase.price
        return total_amount

class UserBase(BaseModel):
    id: int
    name: str


class UserPublic(BaseModel):
    id: int
    name: str
    avatar: str | None


class UserNew(BaseModel):
    name: str
    email: str
    password: str
    moneybird_id: str | None = None


class ChangePassword(BaseModel):
    password: str
    repeat: str

class UserProfile(BaseModel):
    name: str
    email: str
    allowed_prohibited: bool


class UserFull(UserBase):
    model_config = ConfigDict(from_attributes=True)

    email: str = Field(description="Email address for logging in and resetting-password")
    is_active: bool = Field(default=True, description="If the user is active and is possible to login.")
    moneybird_id: str | None = Field(default=None, description="User id known in administration Moneybird")
    roles: dict | None = Field(default=None, description="JSON with roles and the given percentage discount")
    avatar: str | None = Field(default=None, description="Path to profile picture of user.")
    allowed_prohibited: bool = Field(default=False, description="Whether the user is allowed or prohibited.")


class UserPrivate(UserFull):
    total_spend: float
    total_debt: float

def authenticate_user(db: Session, login: OAuth2PasswordRequestForm) -> User | None:
    user: User = db.query(User).filter(User.email == login.username).one_or_none()
    if not user:
        logger.warn(f"STUKS: Login attempt for unknown user: {login.username}")
        return None
    if user.is_active is False:
        logger.warn(f"STUKS: Login attempt for inactive user: {user.email}")
        return None
    if not user.verify_password(login.password):
        logger.warn(f"STUKS: Login attempt for user with incorrect password: {user.email}")
        return None
    return user

def create_user(db: Session, user: UserNew) -> User:
    new_user = User(**user.model_dump(exclude_none=True, exclude={"password"}))
    new_user.set_password(user.password)

    db.add(new_user)
    db.commit()
    db.refresh(new_user)
    return new_user

def user_avatar(db: Session, user_id: int, avatar: str):
    user = db.query(User).filter(User.id == user_id).one_or_none()
    if not user:
        return {'status': 'User not found'}
    
    user.avatar = f"/static/{avatar}"
    db.add(user)
    db.commit()
    return None


def get_all_users(db: Session) -> list[UserFull]:
    return [UserFull(**user.__dict__) for user in db.query(User).all()]


def get_all_users_with_purchases(db: Session) -> list[UserPrivate]:
    return [UserPrivate(**user.__dict__, total_spend=user.spend(), total_debt=user.debt()) for user in db.query(User).all()]


def get_active_users(db: Session) -> list[UserFull]:
    return [UserFull(**user.__dict__) for user in db.query(User).filter_by(is_active=True).all()]


def get_user(db: Session, user_id: int) -> UserPrivate | bool:
    user = db.query(User).filter(User.id == user_id).one_or_none()
    if not user:
        return False
    return UserPrivate(**user.__dict__, total_spend=user.spend(), total_debt=user.debt())


def set_token_by_email(db: Session, email: str, token: str) -> User | None:
    user = db.query(User).filter(User.email == email).one_or_none()
    if not user:
        return None
    user.set_token(token)
    db.add(user)
    db.commit()
    db.refresh(user)
    return user


def get_user_by_token(db: Session, token: str, password: str):
    user = db.query(User).filter(User.reset_token == token).one_or_none()
    if not user:
        return False
    
    if not user.verify_token(token):
        return False
    
    user.set_password(password)

    # Always apply the changes to the database
    db.add(user)
    db.commit()
    return True


def update_profile(db: Session, user_id: int, profile: UserProfile) -> UserFull:
    user = db.query(User).filter(User.id == user_id).one_or_none()
    if not user:
        return False
    
    user.name = profile.name
    user.email = profile.email
    user.allowed_prohibited = profile.allowed_prohibited
    db.add(user)
    db.commit()
    db.refresh(user)
    return UserFull(**user.__dict__)


def set_user_moneybird(db: Session, user_id: int, moneybird_id: str):
    user = db.query(User).filter(User.id == user_id).one_or_none()
    if user:
        user.moneybird_id = moneybird_id
        db.add(user)
        db.commit()


def set_allowed_prohibited(db: Session, user_id: int, state: bool):
    user = db.query(User).filter(User.id == user_id).one_or_none()
    if not user:
        return {'status': 'User is not found'}

    user.allowed_prohibited = state
    db.add(user)
    db.commit()
    db.refresh(user)


def set_user_role(db: Session, user_id: int, role: str):
    user = db.query(User).filter(User.id == user_id).one_or_none()
    if not user:
        return {'status': 'User is not found'}

    if not ROLES.get(role):
        return {'status': f"Role is unknown, use one of these {list(ROLES)}"}
    if not user.roles:
        user.roles = {}
    user.roles = {key: value for key, value in ROLES.items() if key in user.roles or key == role}
    db.add(user)
    db.commit()
    db.refresh(user)


def remove_user_role(db: Session, user_id: int, role: str):
    user = db.query(User).filter(User.id == user_id).one_or_none()
    if not user:
        return {'status': 'User is not found'}

    if not user.roles:
        return {'status': 'No roles to remove'}
    try:
        user.roles = {key:value for key, value in user.roles.items() if key != role}
    except ValueError:
        return {'status': "The certain user don't have this role, so removal is not possible"}
    db.add(user)
    db.commit()
    db.refresh(user)
    return None


def set_user_inactive(db: Session, user_id: int):
    user: User = db.query(User).filter(User.id == user_id).one_or_none()
    if user:
        user.is_active = False
        db.add(user)
        db.commit()
        return True

    return False


def set_user_active(db: Session, user_id: int):
    user: User = db.query(User).filter(User.id == user_id).one_or_none()
    if user:
        user.is_active = True
        db.add(user)
        db.commit()
        return True

    return False


def change_password(password: str, db: Session, user_id: int):
    user = db.query(User).filter(User.id == user_id).one_or_none()
    if not user:
        return {'status': 304, 'message': 'User not found'}
    
    user.set_password(password)
    db.add(user)
    db.commit()
    return True
