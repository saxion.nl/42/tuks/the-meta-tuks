from fastapi import HTTPException
from .database import Base
from sqlalchemy import Column, Integer, DateTime, Float, ForeignKey
from sqlalchemy.orm import relationship
from datetime import datetime
from sqlalchemy.orm import Session
from sqlalchemy.sql import func
from pydantic import BaseModel, Field
from models.product import ProductFull, product_get_by_id
from routes.auth import TokenPayload
from models.user import UserPublic


class Purchase(Base):
    __tablename__ = "purchases"
    id = Column(Integer, primary_key=True)
    date = Column(DateTime, nullable=False, server_default=func.now())
    product_id = Column(Integer, ForeignKey('products.id'), nullable=False)
    user_id = Column(Integer, ForeignKey('users.id'), nullable=False)
    price = Column(Float, nullable=False)
    invoice = Column(Integer, nullable=True)
    
    product = relationship('Product')
    user = relationship('User')


class PurchaseBase(BaseModel):
    product_id: int
    model_config = {'from_attributes': True, 'populate_by_name': True}


class PurchaseFull(BaseModel):
    id: int
    date: datetime
    product_id: int
    user_id: int
    price: float
    invoice: int | None = None


class PurchasePublic(BaseModel):
    date: datetime
    product: ProductFull
    user: UserPublic
    price: float


class PurchaseInvoice(BaseModel):
    total: int = Field(description="Total of number items purchased.")
    price: float = Field(description="Total amount spend.")


def purchase_user(db: Session, user_id: int, limit) -> list[Purchase] | None:
    return db.query(Purchase).filter_by(user_id=user_id).order_by(Purchase.date.desc()).limit(limit).all()


def purchase_all(db: Session, limit) -> list[Purchase] | None:

    return db.query(Purchase).order_by(Purchase.date.desc()).limit(limit).all()


def purchase_user_not_paid(db: Session, user_id: int) -> list[Purchase]:
    """Returns a list of purchases from given user with only unpaid items"""
    return db.query(Purchase).filter(Purchase.user_id == user_id, Purchase.invoice.is_(None)).order_by(Purchase.date.desc()).all()


def purchase_product(db: Session, user: TokenPayload, product_id: int) -> Purchase:
    product = product_get_by_id(db, product_id)
    
    if not product or not user:
        raise HTTPException(status_code=404, detail="Product not found")
    
    if not user["allowed_prohibited"] and product.prohibited:
        raise HTTPException(status_code=403, detail="You are not allowed to buy prohibited products")

    purchase = Purchase()
    purchase.product_id = product.id
    purchase.user_id = user['id']
    purchase.price = round(product.price * user['discount'], 2)
    purchase.date = datetime.now()
    db.add(purchase)

    product.stock -= 1
    db.add(product)
    
    db.commit()
    db.refresh(purchase)
    return purchase


def purchases_unpaid_by_user(db: Session, user_id: int) -> [ProductFull]:
    purchases = db.query(Purchase).filter(Purchase.user_id == user_id, Purchase.invoice == None)
    return [PurchaseFull(**purchase.__dict__) for purchase in purchases]


def mark_unpaid_by_user_as_paid(db: Session, user_id: int, invoice) -> PurchaseInvoice:
    purchases = db.query(Purchase).filter(Purchase.user_id == user_id, Purchase.invoice.is_(None))
    amount = 0.00
    total = 0
    for pur in purchases:
        pur.invoice = invoice
        amount += pur.price
        db.add(pur)
        db.commit()
        total += 1
    return PurchaseInvoice(total=total, price=amount) # type: ignore
