from sqlalchemy import create_engine
from utils import getenv
from sqlalchemy.orm import Session, declarative_base, sessionmaker

# CONSTANTS
DATABASE_URI = f"mariadb://{getenv('DATABASE_USERNAME')}:{getenv('DATABASE_PASSWORD')}@{getenv('DATABASE_SERVER')}:{getenv('DATABASE_PORT')}/{getenv('DATABASE_NAME')}"

engine = create_engine(DATABASE_URI, pool_pre_ping=True)

SessionLocal = sessionmaker(autoflush=False, autocommit=False, bind=engine)

def get_session():
    db = SessionLocal()
    try:
        yield db
    finally:
        db.close()


Base = declarative_base()
