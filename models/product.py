from .database import Base
from sqlalchemy import Column, Integer, Text, Boolean, Float, Enum
from sqlalchemy.orm import Session
from pydantic import BaseModel, Field
from utils import get_logger

class Product(Base):
    __tablename__ = "products"
    
    id = Column(Integer, primary_key=True)
    name = Column(Text, nullable=False)
    price = Column(Float, default=0.0)
    stock = Column(Integer, default=0)
    ean_code = Column(Text, nullable=True)
    is_active = Column(Boolean, default=True) # Used to disable the product but still remain in the database for purchase-history.
    prohibited = Column(Boolean, default=False)
    type = Column(Enum('drink', 'snack', 'other'), default='drink', nullable=False)
    picture = Column(Text, nullable=True)


class ProductBase(BaseModel):
    name: str
    price: float
    type: str
    ean_code: str
    prohibited: bool
    model_config = {'from_attributes': True, 'populate_by_name': True}


class ProductCreate(BaseModel):
    name: str = Field(description="Name of the product.")
    price: float = Field(description="Price of the product in a float")
    prohibited: bool = Field(description="18+ = true or 18- = false")
    is_active: bool = Field(description="Active = true or Inactive = false")
    ean_code: str = Field(description="EAN code of the product.")
    type: str = Field(default='drink', description="Choose one of type's: drink, snack, other")


class ProductFull(BaseModel):
    id: int
    name: str
    price: float
    is_active: bool
    stock: int
    type: str
    prohibited: bool
    ean_code: str | None
    model_config = {'from_attributes': True}
    picture: str | None


class ProductStockUpdate(BaseModel):
    id: int
    amount: int


def products_active(session: Session, allowed_prohibited: bool = True) -> list[Product]:
    query = session.query(Product).filter(Product.is_active == True)
    if not allowed_prohibited:
        query = query.filter(Product.prohibited == False)
        
    return query.order_by(Product.name).all()



def all_products(db: Session) -> list[Product]:
    return db.query(Product).all()


def product_stock_update(db: Session, product_stock_update: ProductStockUpdate, current_user) -> None | int:
    logger = get_logger()
    product = db.query(Product).filter(Product.id == product_stock_update.id).one_or_none()
    if not product:
        return None
    old_stock = product.stock
    product.stock = product.stock + product_stock_update.amount
    logger.info(msg=f"STOCK {current_user.get('name')} set {product.name} from {old_stock} to {product.stock}")
    db.add(product)
    db.commit()

    return product.stock

def product_create(db: Session, new_product: ProductCreate) -> Product:
    product = Product(**new_product.model_dump(exclude_none=True))
    db.add(product)
    db.commit()
    db.refresh(product)
    return product


def product_update_picture(db: Session, product_id: int, picture: str):
    product = db.query(Product).filter(Product.id == product_id).one_or_none()
    if not product:
        return False
    product.picture = picture
    db.add(product)
    db.commit()

    return True


def product_get_by_id(db: Session, product_id: int) -> Product | None:
    """Retrieve a product based on ID

    Args:
        db (Session): the current session connected to the database
        product_id (int): id of the product to retrieve

    Returns:
        Product | None: a SqlAclhemy object or None if the item is not found.
    """
    return db.query(Product).filter(Product.id == product_id).first()


def product_update(db: Session, product_id: int, product: ProductFull) -> Product | None:
    prod = product_get_by_id(db, product_id)

    if prod:
        for key, value in product.model_dump(exclude_none=True).items():
            setattr(prod, key, value) if value is not None else None
        db.add(prod)
        db.commit()
        db.refresh(prod)
    
    return prod


def product_enable(db: Session, product_id: int) -> bool:
    prod: Product = db.query(Product).filter(Product.id == product_id).one_or_none()
    if prod:
        prod.is_active = True
        db.add(prod)
        db.commit()
        return True

    return False


def product_disable(db: Session, product_id: int) -> bool:
    prod = db.query(Product).filter(Product.id == product_id).one_or_none()
    if prod:
        prod.is_active = False
        db.add(prod)
        db.commit()
        return True

    return False
