"""Add allowed_prohibited to users

Revision ID: e113f2c5049f
Revises: fe9d48f93934
Create Date: 2024-11-21 23:21:52.519399

"""
from typing import Sequence, Union

from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision: str = 'e113f2c5049f'
down_revision: Union[str, None] = 'fe9d48f93934'
branch_labels: Union[str, Sequence[str], None] = None
depends_on: Union[str, Sequence[str], None] = None


def upgrade() -> None:
    # ### commands auto generated by Alembic - please adjust! ###
    op.add_column('products', sa.Column('prohibited', sa.Boolean(), nullable=True))
    # ### end Alembic commands ###


def downgrade() -> None:
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_column('products', 'prohibited')
    # ### end Alembic commands ###
