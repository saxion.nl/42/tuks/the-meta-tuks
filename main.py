from fastapi import FastAPI
from fastapi.staticfiles import StaticFiles
from models.database import Base, engine
from routes.product import product_router
from routes.administration import administration_router
from routes.auth import auth_router
from routes.user import user_router
from routes.me import me_router
from routes.purchase import purchase_router
from utils import getenv

app = FastAPI(title="Stuks-API")

# Static files
app.mount('/static', StaticFiles(directory='static'), name='static')

# Environment automatic populate
if getenv('ENVIRONMENT') in {'local', 'test'}:
    from routes.dev import dev_router
    app.include_router(dev_router)
    app.debug = True

# Routers
app.include_router(me_router)
app.include_router(product_router)
app.include_router(auth_router)
app.include_router(user_router)
app.include_router(administration_router)
app.include_router(purchase_router)
